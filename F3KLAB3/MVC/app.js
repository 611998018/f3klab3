
const express = require('express');
const body = require('body-parser')
const cookie = require('cookie-parser');
const session = require('express-session');
const mysql = require('mysql');
const connection = require('express-myconnection');
const app = express();

app.use(express.static('public'));
app.set('view engine', 'ejs');
app.use(body.urlencoded({exteded:true}));
app.use(cookie());
app.use(session({
secret:'Passw0rd',
resave:true,
saveUnintialized:true
}));


app.use(connection(mysql, {
    host: 'localhost',
    user: 'root',
    password: '1234',
    port: 3306,
    database: 'f3k3'
}, 'single'));



const loginRoute = require('./routes/f3k3Route');
 app.use('/', loginRoute);

const login = require('./routes/ce611998039Route');
app.use('/', login);

const wit = require('./routes/ce611998018Route');
app.use('/', wit);

const win = require('./routes/ce611998025Route');
app.use('/', win);

const arm = require('./routes/ce611998032Route');
app.use('/', arm);



app.listen(8081);
