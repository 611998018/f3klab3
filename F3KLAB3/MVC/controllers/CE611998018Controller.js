const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
  if(req.session.user ){

    req.getConnection((err,conn) =>{
        conn.query('select w.id as wid,w.scit611998018 as wscit611998018,w.mirot611998018 as wmirot611998018,t.id as tid,t.scit611998025 as tscit611998025,t.mirot611998025 as tmirot611998025,c.id as cid,c.scit611998032 as cscit611998032 ,c.mirot611998032 as cmirot61198032 from ce611998018 as w left join ce611998025 as t on w.ID_CE611998025 = t.id left join ce611998032 as c on w.ID_CE611998032 = c.id ',(err,ce611998018) =>{
            if(err){
                res.json(err);
            }
            res.render('CE611998018/CE611998018List',{session: req.session,data:ce611998018});
        });
    });
  
}else {
    res.redirect('/');
  }
};

controller.save = (req,res) => {
  if(req.session.user && req.session.modulece611998018==1 ){
    const data=req.body;
     if(data.ID_CE611998025==""){data.ID_CE611998025=null;}
      if(data.ID_CE611998032==""){data.ID_CE611998032=null;}

    const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/ce611998018/add');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn)=>{
            conn.query('INSERT INTO ce611998018 set ?',[data],(err,ce611998018)=>{
            res.redirect('/ce611998018');
            });
        });
    };
}else {
    res.redirect('/');
  }
};

controller.del = (req,res) => {
    if(req.session.user && req.session.modulece611998018==1 ){
    const { id } = req.params;

    req.getConnection((err,conn)=>{
        conn.query('select w.id as wid,w.scit611998018 as wscit611998018,w.mirot611998018 as wmirot611998018,t.id as tid,t.scit611998025 as tscit611998025,t.mirot611998025 as tmirot611998025,c.id as cid,c.scit611998032 as cscit611998032 ,c.mirot611998032 as cmirot61198032 from ce611998018 as w left join ce611998025 as t on w.ID_CE611998025 = t.id left join ce611998032 as c on w.ID_CE611998032 = c.id HAVING w.id = ?',[id],(err,ce611998018)=>{
          if(err){
              res.json(err);
          }
          res.render('CE611998018/CE611998018Delete',{
            session: req.session,
            data:ce611998018[0]});
      });
  });
}else {
    res.redirect('/');
  };
};

controller.delete = (req,res) => {
    if(req.session.user && req.session.modulece611998018==1 ){
    const { id } = req.params;

    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM ce611998018 WHERE id= ?',[id],(err,ce611998018)=>{
            if(err){
              const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
              req.session.errors=errorss;
              req.session.success=false;
            }else {
                req.session.success=true;
                req.session.topic="ลบข้อมูลเสร็จแล้ว";
            }
            console.log(ce611998018);
            res.redirect('/ce611998018');
        });
    });
}else {
    res.redirect('/');
  }
};

controller.edit = (req,res) => {
    if(req.session.user && req.session.modulece611998018==1 ){
    const { id } = req.params;

            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM ce611998018 WHERE id= ?',[id],(err,ce611998018)=>{
                    conn.query('SELECT * FROM ce611998025',(err,ce611998025)=>{
                      conn.query('SELECT * FROM ce611998032',(err,ce611998032)=>{
                        res.render('CE611998018/CE611998018Update',{
                          session: req.session,
                          data1:ce611998018[0],
                          data2:ce611998025,
                          data3:ce611998032});
                        });
                });
            });
        });
      }
    }

controller.update = (req,res) => {
    if(req.session.user && req.session.modulece611998018==1 ){
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
    if(data.ID_CE611998025==""){data.ID_CE611998025=null;}
     if(data.ID_CE611998032==""){data.ID_CE611998032=null;}

    console.log(data);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM ce611998018 WHERE id= ?',[id],(err,ce611998018)=>{
                conn.query('SELECT * FROM ce611998025',(err,ce611998025)=>{
                conn.query('SELECT * FROM ce611998032',(err,ce611998032)=>{
                res.render('CE611998018/CE611998018Update',{
                  session: req.session,
                  data1:ce611998018[0],
                  data2:ce611998025,
                  data3:ce611998032});

                });
                });
              });
            });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn) => {
                conn.query('UPDATE  ce611998018 SET ?  WHERE id = ?',[data,id],(err,ce611998018) => {
                  if(err){
                      res.json(err);
                  }
               res.redirect('/ce611998018');
               });
             });
        }
}else {
    res.redirect('/');
  }
};

controller.add = (req,res) => {
    if(req.session.user && req.session.modulece611998018==1 ){
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM ce611998025',(err,ce611998025) =>{
      conn.query('SELECT * FROM ce611998032',(err,ce611998032) =>{
      res.render('CE611998018/CE611998018Add',{
        data:ce611998025,
        data1:ce611998032,
        session: req.session});

      });
    });
  });
};
};
module.exports = controller;
