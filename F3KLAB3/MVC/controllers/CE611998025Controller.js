const controller ={};
const { validationResult } = require('express-validator');
controller.list = (req,res) => {
    if(req.session.user ){

        req.getConnection((err,conn) =>{
            conn.query('SELECT win.id winId, scit611998025, mirot611998025, id_ce611998032, win.id_ce611998039 id_ce611998039s, scit611998032, scit611998039 FROM ce611998025 win left join ce611998032 arm on win.id_ce611998032 = arm.id left join ce611998039 wut on win.id_ce611998039 = wut.id',(err,s6125) =>{
                if(err){
                    res.json(err);
                }
                res.render('ce611998025/ce611998025List',{session: req.session,data:s6125});
            });
        });
    
    }else {
        res.redirect('/');
      }

};
controller.save = (req,res) => {
    const data=req.body;
    if(data.ID_CE611998032==""){data.ID_CE611998032 = null;}
    if(data.ID_CE611998039==""){data.ID_CE611998039 = null;}
    // console.log(data.id_chanchai);
    // console.log(data);
    const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/ce611998025/add');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";

            if(req.session.user & req.session.modulece611998025==1){
                req.getConnection((err,conn)=>{
                    conn.query('INSERT INTO ce611998025 set ?',[data],(err,s6125)=>{
                        res.redirect('/ce611998025');
                    });
                });
            }else {
                res.redirect('/');
              }

        }
};
controller.del = (req,res) => {
    const { id } = req.params;
    if(req.session.user & req.session.modulece611998025==1){
        req.getConnection((err,conn)=>{
            conn.query('SELECT win.id winId, scit611998025, mirot611998025, id_ce611998032, win.id_ce611998039 id_ce611998039s, scit611998032, scit611998039 FROM ce611998025 win left join ce611998032 arm on win.id_ce611998032 = arm.id left join ce611998039 wut on win.id_ce611998039 = wut.id HAVING win.id= ?;',[id],(err,s6125)=>{/////Update
                if(err){
                    res.json(err);
                }
                res.render('ce611998025/ce611998025Delete',{session: req.session,data:s6125[0]});
            });
        });
    }else {
    res.redirect('/');
  }

};
controller.delete = (req,res) => {
    const { id } = req.params;
    if(req.session.user & req.session.modulece611998025==1){
    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM ce611998025 WHERE id= ?',[id],(err,s6125)=>{
            if(err){
              const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
              req.session.errors=errorss;
              req.session.success=false;
            }else {
                req.session.success=true;
                req.session.topic="ลบข้อมูลเสร็จแล้ว";
            }
            console.log(s6125);
            res.redirect('/ce611998025');
        });
    });
}else {
    res.redirect('/');
  }
};

controller.edit = (req,res) => {
    const { id } = req.params;
    if(req.session.user & req.session.modulece611998025==1){
        req.getConnection((err,conn)=>{
            conn.query('SELECT * FROM ce611998025 WHERE id= ?',[id],(err,s6125)=>{
                conn.query('SELECT * FROM ce611998032',(err,s6132)=>{
                    conn.query('SELECT * FROM ce611998039',(err,s6139)=>{
                        res.render('ce611998025/ce611998025Update',{
                            session: req.session,
                            data1:s6125[0],
                            data2:s6132,
                            data3:s6139});
                    });
                });
            });
        });
    }else {
        res.redirect('/');
      }

        }
controller.update = (req,res) => {
  if(req.session.user && req.session.modulece611998025==1 ){
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
    if(data.ID_CE611998032==""){data.ID_CE611998032 = null;}
    if(data.ID_CE611998039==""){data.ID_CE611998039 = null;}
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;

                req.getConnection((err,conn)=>{
                    conn.query('SELECT * FROM ce611998025 WHERE ID= ?',[id],(err,s6125)=>{
                        conn.query('SELECT * FROM ce611998032',(err,s6132)=>{
                            conn.query('SELECT * FROM ce611998039',(err,s6139)=>{
                                res.render('ce611998025/ce611998025Update',{
                                    session: req.session,
                                    data1:s6125[0],
                                    data2:s6132,
                                    data3:s6139});
                            });
                        });
                    });
                });


        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";

                req.getConnection((err,conn) => {
                    conn.query('UPDATE  ce611998025 SET ?  WHERE ID = ?',[data,id],(err,s6125) => {
                      if(err){
                          res.json(err);
                      }
                   res.redirect('/ce611998025');
                   });
                 });


        }
      }  else {
            res.redirect('/');
          }
        }


controller.add = (req,res) => {
    if(req.session.user & req.session.modulece611998025==1){
        req.getConnection((err,conn) => {
            conn.query('SELECT * FROM ce611998032',(err,s6132) =>{
              conn.query('SELECT * FROM ce611998039',(err,s6139) =>{
                  res.render('ce611998025/ce611998025Add',{data1:s6132,data2:s6139,session: req.session});
              });
            });
          });
    }
    else {
        res.redirect('/');
      }

  };

module.exports = controller;
