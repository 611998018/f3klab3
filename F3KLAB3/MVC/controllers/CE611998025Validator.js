const { check } = require('express-validator');
exports.addValidator = [
    check('SCIT611998025',"SCIT611998025 ไม่ถูกต้อง").not().isEmpty(),
    check('MIROT611998025',"MIROT611998025 ไม่ถูกต้อง").isFloat()];
exports.updateValidator = [
    check('SCIT611998025',"SCIT611998025 ไม่ถูกต้อง").not().isEmpty(),
    check('MIROT611998025',"MIROT611998025 ไม่ถูกต้อง").isFloat()];
