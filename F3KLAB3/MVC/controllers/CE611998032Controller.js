const controller ={};
const { validationResult } = require('express-validator');
controller.list = (req,res) => {
    if(req.session.user ){

        req.getConnection((err,conn) =>{
            conn.query('SELECT arm.id armId, scit611998032, mirot611998032, id_ce611998039, arm.id_ce611998018 id_ce611998018s, scit611998039, scit611998018 FROM ce611998032 arm left join ce611998039 wut on arm.id_ce611998039 = wut.id left join ce611998018 wit on arm.id_ce611998018 = wit.id',(err,s6132) =>{
                if(err){
                    res.json(err);
                }
                res.render('ce611998032/ce611998032List',{session: req.session,data:s6132});
            });
        });
    
    }else {
        res.redirect('/');
      }

};
controller.save = (req,res) => {
    if(req.session.user && req.session.modulece611998032==1 ){
    const data=req.body;
    if(data.ID_CE611998039==""){data.ID_CE611998039 = null;}
    if(data.ID_CE611998018==""){data.ID_CE611998018 = null;}
    // console.log(data.id_chanchai);
    // console.log(data);
    const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/ce611998032/add');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";


                req.getConnection((err,conn)=>{
                    conn.query('INSERT INTO ce611998032 set ?',[data],(err,s6132)=>{
                        res.redirect('/ce611998032');
                    });
                });


        }
      }
      else {
      res.redirect('/');
    }
};
controller.del = (req,res) => {
    if(req.session.user && req.session.modulece611998032==1 ){
    const { id } = req.params;

        req.getConnection((err,conn)=>{
            conn.query('SELECT arm.id armId, scit611998032, mirot611998032, id_ce611998039, arm.id_ce611998018 id_ce611998018s, scit611998039, scit611998018 FROM ce611998032 arm left join ce611998039 wut on arm.id_ce611998039 = wut.id left join ce611998018 wit on arm.id_ce611998018 = wit.id HAVING arm.id= ?;',[id],(err,s6132)=>{/////Update
                if(err){
                    res.json(err);
                }
                res.render('ce611998032/ce611998032Delete',{session: req.session,data:s6132[0]});
            });
        });
    }else {
    res.redirect('/');
  }

};
controller.delete = (req,res) => {
    if(req.session.user && req.session.modulece611998032==1 ){
    const { id } = req.params;

    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM ce611998032 WHERE id= ?',[id],(err,s6132)=>{
            if(err){
              const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
              req.session.errors=errorss;
              req.session.success=false;
            }else {
                req.session.success=true;
                req.session.topic="ลบข้อมูลเสร็จแล้ว";
            }
            console.log(s6132);
            res.redirect('/ce611998032');
        });
    });
}else {
    res.redirect('/');
  }
};

controller.edit = (req,res) => {
    if(req.session.user && req.session.modulece611998032==1 ){
    const { id } = req.params;

        req.getConnection((err,conn)=>{
            conn.query('SELECT * FROM ce611998032 WHERE id= ?',[id],(err,s6132)=>{
                conn.query('SELECT * FROM ce611998039',(err,s6139)=>{
                    conn.query('SELECT * FROM ce611998018',(err,s6118)=>{
                        res.render('ce611998032/ce611998032Update',{
                            session: req.session,
                            data1:s6132[0],
                            data2:s6139,
                            data3:s6118});
                    });
                });
            });
        });
    }else {
        res.redirect('/');
      }

        }
controller.update = (req,res) => {
    if(req.session.user && req.session.modulece611998032==1 ){
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
    if(data.ID_CE611998039==""){data.ID_CE611998039 = null;}
    if(data.ID_CE611998018==""){data.ID_CE611998018 = null;}
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;

                req.getConnection((err,conn)=>{
                    conn.query('SELECT * FROM ce611998032 WHERE ID= ?',[id],(err,s6132)=>{
                        conn.query('SELECT * FROM ce611998039',(err,s6139)=>{
                            conn.query('SELECT * FROM ce611998018',(err,s6118)=>{
                                res.render('ce611998032/ce611998032Update',{
                                    session: req.session,
                                    data1:s6132[0],
                                    data2:s6139,
                                    data3:s6118});
                            });
                        });
                    });
                });


        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";

                req.getConnection((err,conn) => {
                    conn.query('UPDATE  ce611998032 SET ?  WHERE ID = ?',[data,id],(err,s6132) => {
                      if(err){
                          res.json(err);
                      }
                   res.redirect('/ce611998032');
                   });
                 });


        }

}else {
    res.redirect('/');
  }
}
controller.add = (req,res) => {
      if(req.session.user && req.session.modulece611998032==1 ){
        req.getConnection((err,conn) => {
            conn.query('SELECT * FROM ce611998039',(err,s6139) =>{
              conn.query('SELECT * FROM ce611998018',(err,s6118) =>{
                  res.render('ce611998032/ce611998032Add',{data1:s6139,data2:s6118,session: req.session});
              });
            });
          });
    }
    else {
        res.redirect('/');
      }

  };

module.exports = controller;
