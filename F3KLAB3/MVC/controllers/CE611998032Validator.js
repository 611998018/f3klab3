const { check } = require('express-validator');
exports.addValidator = [
    check('SCIT611998032'," SCIT611998032 ไม่ถูกต้อง").not().isEmpty(),
    check('MIROT611998032',"MIROT611998032 ไม่ถูกต้อง").isFloat()];
exports.updateValidator = [
    check('SCIT611998032',"SCIT611998032 ไม่ถูกต้อง").not().isEmpty(),
    check('MIROT611998032',"MIROT611998032 ไม่ถูกต้อง").isFloat()];
