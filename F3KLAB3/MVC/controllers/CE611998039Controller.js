const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
  if(req.session.user){

    req.getConnection((err,conn) =>{
        conn.query('select w.id as wid,w.scit611998039 as wscit611998039,w.mirot611998039 as wmirot611998039,t.id as tid,t.scit611998018 as tscit611998018,t.mirot611998018 as tmirot611998018,c.id as cid,c.scit611998025 as cscit611998025 ,c.mirot611998025 as cmirot61198025 from ce611998039 as w left join ce611998018 as t on w.id_ce611998018 = t.id left join ce611998025 as c on w.id_ce611998025 = c.id ',(err,ce611998039) =>{
            if(err){
                res.json(err);
            }
            res.render('CE611998039/CE611998039List',{session: req.session,data:ce611998039});
        });
    });
  
}else {
    res.redirect('/');
  }
};

controller.save = (req,res) => {
  if(req.session.user && req.session.modulece611998039==1){
    const data=req.body;
     if(data.ID_CE611998018==""){data.ID_CE611998018=null;}
      if(data.ID_CE611998025==""){data.ID_CE611998025=null;}

    const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/ce611998039/add');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn)=>{
            conn.query('INSERT INTO ce611998039 set ?',[data],(err,ce611998039)=>{
            res.redirect('/ce611998039');
            });
        });
    };
}else {
    res.redirect('/');
  }
};

controller.del = (req,res) => {
  if(req.session.user && req.session.modulece611998039==1){
    const { id } = req.params;

    req.getConnection((err,conn)=>{
        conn.query('select w.id as wid,w.scit611998039 as wscit611998039,w.mirot611998039 as wmirot611998039,t.id as tid,t.scit611998018 as tscit611998018,t.mirot611998018 as tmirot611998018,c.id as cid,c.scit611998025 as cscit611998025 ,c.mirot611998025 as cmirot61198025 from ce611998039 as w left join ce611998018 as t on w.id_ce611998018 = t.id left join ce611998025 as c on w.id_ce611998025 = c.id HAVING w.id = ?',[id],(err,ce611998039)=>{
          if(err){
              res.json(err);
          }
          res.render('CE611998039/CE611998039Delete',{session: req.session,data:ce611998039[0]});
      });
  });
}else {
    res.redirect('/');
  };
};

controller.delete = (req,res) => {
    if(req.session.user && req.session.modulece611998039==1){
    const { id } = req.params;

    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM ce611998039 WHERE id= ?',[id],(err,ce611998039)=>{
            if(err){
              const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
              req.session.errors=errorss;
              req.session.success=false;
            }else {
                req.session.success=true;
                req.session.topic="ลบข้อมูลเสร็จแล้ว";
            }
            console.log(ce611998039);
            res.redirect('/ce611998039');
        });
    });
}else {
    res.redirect('/');
  }
};

controller.edit = (req,res) => {
    if(req.session.user && req.session.modulece611998039==1){
    const { id } = req.params;

            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM ce611998039 WHERE id= ?',[id],(err,ce611998039)=>{
                    conn.query('SELECT * FROM ce611998018',(err,ce611998018)=>{
                      conn.query('SELECT * FROM ce611998025',(err,ce611998025)=>{
                        res.render('CE611998039/CE611998039Update',{
                          session: req.session,
                          data1:ce611998039[0],
                          data2:ce611998018,
                          data3:ce611998025});
                        });
                });
            });
        });
      }else {
          res.redirect('/');
        }
    }

controller.update = (req,res) => {
  if(req.session.user && req.session.modulece611998039==1){
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
    if(data.ID_CE611998018==""){data.ID_CE611998018=null;}
     if(data.ID_CE611998025==""){data.ID_CE611998025=null;}


        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;

            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM ce611998039 WHERE id= ?',[id],(err,ce611998039)=>{
                conn.query('SELECT * FROM ce611998018',(err,ce611998018)=>{
                conn.query('SELECT * FROM ce611998025',(err,ce611998025)=>{
                res.render('CE611998039/CE611998039Update',{
                  session: req.session,
                  data1:ce611998039[0],
                  data2:ce611998018,
                  data3:ce611998025});

                });
                });
              });
            });

        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn) => {
                conn.query('UPDATE  ce611998039 SET ?  WHERE id = ?',[data,id],(err,ce611998039) => {
                  if(err){
                      res.json(err);
                  }
               res.redirect('/ce611998039');
               });
             });
        }
        }else {
    res.redirect('/');
  }
};

controller.add = (req,res) => {
  if(req.session.user && req.session.modulece611998039==1){
    req.getConnection((err,conn) => {
      conn.query('SELECT * FROM ce611998018',(err,ce611998018) =>{
      conn.query('SELECT * FROM ce611998025',(err,ce611998025) =>{
      res.render('CE611998039/CE611998039Add',{data:ce611998018,data1:ce611998025,session: req.session});

      });
    });
  });
}else {
    res.redirect('/');
  }
};
module.exports = controller;
