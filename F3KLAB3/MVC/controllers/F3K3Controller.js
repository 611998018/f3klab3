const controller = {};
const { validationResult } = require('express-validator');
const { default: validator } = require('validator');


controller.log = (req, res) => {
    res.render('login', { session: req.session });
};


controller.home = function(req, res) {
    if (req.session.user) {
        res.render('index', { session: req.session });
    } else {
        res.redirect('/');
    }
};


controller.logout = function(req, res) {
    req.session.destroy();
    res.redirect('/');
};


 controller.login = (req, res) => {
   const errors = validationResult(req);
          if(!errors.isEmpty()){
              req.session.errors = errors;
              req.session.success = false;
              res.redirect('/');
          }else{
              req.session.success=true;
              req.session.topic="Login ไม่สำเร็จ";
              const username = req.body.username;
              const password = req.body.password;
              req.getConnection((err,conn) =>{
              conn.query('SELECT * FROM user WHERE username = ? AND password = ?',[username,password],(err,data) =>{
              if (err) {
              res.JSON(err);
              }else {
               if (data.length > 0) {
               req.session.user = data[0].id;
               req.session.topic="Login สำเร็จ";
           conn.query('SELECT * FROM permission WHERE module_id=1 AND user_id=?',[req.session.user],(err2,modulece611998025)=>{
              conn.query('SELECT * FROM permission WHERE module_id=2 AND user_id=?',[req.session.user],(err2,modulece611998032)=>{
                 conn.query('SELECT * FROM permission WHERE module_id=3 AND user_id=?',[req.session.user],(err2,modulece611998039)=>{
                    conn.query('SELECT * FROM permission WHERE module_id=4 AND user_id=?',[req.session.user],(err2,modulece611998018)=>{
                      conn.query('SELECT * FROM permission WHERE module_id=5 AND user_id=?',[req.session.user],(err2,moduleadmin)=>{
             if(modulece611998025.length > 0){
               req.session.modulece611998025 = 1;
             }else {
                req.session.modulece611998025 = 0;
             }
             if(modulece611998032.length > 0){
               req.session.modulece611998032 = 1;
             }else {
                req.session.modulece611998032 = 0;
             }
             if(modulece611998039.length > 0){
               req.session.modulece611998039 = 1;
             }else {
                req.session.modulece611998039 = 0;
             }
             if(modulece611998018.length > 0){
               req.session.modulece611998018 = 1;
             }else {
                req.session.modulece611998018 = 0;
             }
             if(moduleadmin.length > 0){
               req.session.moduleadmin = 1;
                req.session.modulece611998025 = 1;
               req.session.modulece611998032 = 1;
               req.session.modulece611998039 = 1;
                 req.session.modulece611998018 = 1;
             }else {
                req.session.moduleadmin = 0;
             }
               res.redirect('/home');
           });


            });

           });
});
});
        }else {
          res.redirect('/');
        }
      }
    })
  })
};

};







// controller.logi = function(req,res){
//     res.render("../views/index",{session:req.session});
// }

module.exports = controller;
