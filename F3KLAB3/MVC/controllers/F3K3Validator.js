const { check } = require('express-validator');

exports.loginValidator = [check('username', "username ไม่ถูกต้อง").not().isEmpty(),
                         check('password', "password ไม่ถูกต้อง").not().isEmpty()  ];
