const express = require('express');
const router = express.Router();
const studentToller = require('../controllers/ce611998018Controller');
const validator = require('../controllers/ce611998018Validator');

router.get('/ce611998018',studentToller.list);
router.post('/ce611998018/save',validator.addValidator,studentToller.save);
router.get('/ce611998018/delete/:id',studentToller.delete);
router.get('/ce611998018/del/:id',studentToller.del);
router.get('/ce611998018/update/:id',studentToller.edit);
router.post('/ce611998018/update/:id',validator.updateValidator,studentToller.update);
router.get('/ce611998018/add',studentToller.add);

module.exports = router;
