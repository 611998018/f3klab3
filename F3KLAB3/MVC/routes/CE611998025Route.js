const express = require('express');
const router = express.Router();
const ce6125Controller = require('../controllers/CE611998025Controller');
const ce6125Validator = require('../controllers/CE611998025Validator');
router.get('/ce611998025',ce6125Controller.list);

router.get('/ce611998025/add',ce6125Controller.add);

router.post('/ce611998025/save',ce6125Validator.addValidator,ce6125Controller.save);

router.get('/ce611998025/del/:id',ce6125Controller.del);

router.get('/ce611998025/delete/:id',ce6125Controller.delete);

router.get('/ce611998025/edit/:id',ce6125Controller.edit);

router.post('/ce611998025/update/:id',ce6125Validator.updateValidator,ce6125Controller.update);

module.exports = router;
