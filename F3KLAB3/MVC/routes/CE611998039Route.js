const express = require('express');
const router = express.Router();
const studentToller = require('../controllers/ce611998039Controller');
const validator = require('../controllers/ce611998039Validator');

router.get('/ce611998039',studentToller.list);
router.post('/ce611998039/save',validator.addValidator,studentToller.save);
router.get('/ce611998039/delete/:id',studentToller.delete);
router.get('/ce611998039/del/:id',studentToller.del);
router.get('/ce611998039/update/:id',studentToller.edit);
router.post('/ce611998039/update/:id',validator.updateValidator,studentToller.update);
router.get('/ce611998039/add',studentToller.add);

module.exports = router;
