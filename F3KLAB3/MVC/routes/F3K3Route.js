const express = require('express');
const router = express.Router();

const f3k3Controller = require('../controllers/f3k3Controller');
  const validator = require('../controllers/f3k3Validator');

router.get('/', f3k3Controller.log);
//router.post('/login', validator.loginValidator, f3k3Controller.login);
router.post('/login', validator.loginValidator,f3k3Controller.login);
router.get('/home', f3k3Controller.home);
router.get('/logout', f3k3Controller.logout);

// router.get('/home',f3k3Controller.logi);




module.exports = router;
